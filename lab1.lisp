;;Задание 1
;;(append (append (cdr '(REM FFG HHJ (H J G D))) (cdr '(2 34 56 78 (7 8))))  (cdr '(UN Y LOOP)))
(list (cdr '(REM FFG HHJ (H J G D))) (cdr '(2 34 56 78 (7 8))) (cdr '(UN Y LOOP)))
;;Задание 2
(list (fourth '(REM FFG HHJ (H J G D))) (fourth '(2 34 56 78 (7 8))) (second '(UN Y LOOP)) )
;;Задание 3
(defun task3 (elements)
    (loop for el in elements
          when (numberp el) collect el
            )
    )
(setq a (car '((H J G D) 78 Y)))
(setq b (second '((H J G D) 78 Y)))
(setq c (third '((H J G D) 78 Y)))
(EQUAL a b)

;;Задание 4
(CADDDR '(1 2 3 a 4))
(CAR (CDDDDR '(1 2 3 4 a)))
(CAADDR '((1) (2 3) (а 4)))
(CADR (CDAADR '((1) ((2 3 а) (4)))))
(CADR (CDAADR '((1) ((2 3 а 4)))))
(CDADR (CADDAR (CADADR '(1 (2 ((3 4 (5 (6 а)))))))))
;;Задание 5
(setq vasya '(("11.12.2014") ("Baker st") ((4.5) (5.0)(4.1)) ((("Andrey")("01.11.1963")("Baker st")("SFU"))(("Masha")("01.06.1963")("Baker st")("AIS")))))
(setq natasha '(("11.12.2015") ("Svobonyi st") ((4.7) (5.0)(4.8)) ((("Alex")("01.11.1956")("Svobonyi st")("KGB"))(("Olga")("01.06.19659")("Svobonyi st")("Labs core")))))
(list vasya natasha)
(((дата рождения ст.) (адрес ст.)((ср. бал(до десятых) по лекционным занятиям) (ср. бал по практическим занятиям) (ср. бал по лабораторным работам))) (((ФИО отца) (дата рождения отца) (адрес) (место работы отца)) ((ФИО матери) (дата рождения матери) (адрес) (место работы матери))))
