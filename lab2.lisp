;;Задание 1
_((lambda (x y z) ((list (cdr x) (cdr y) (cdr z)))) '(REM FFG HHJ (H J G D)) '(1 2 3) '(456 6 sss))

;; Задание 2
(defun task2 (x y z)
    (list (fourth x) (fourth Y) (second z ))
    )
(defun task2_2 (list1 list2)
    (if (>= (*(car list1) (car list2)) 0 )
        (list (car list1) (car list2))
        (append (cdr list1) (cdr list2))
        )
    )
;;Задание 3
(defun NULL1 (x)
    (eq  x '()))
(defun CADDR1 (x)
    (car (cdr (cdr x))))
(defun LIST1 (x1 x2 x3)
    (cons x1 (cons x2 (cons x3 nil)))
    )
;;Задание 4
(defun task4 (l)
    (if (null l)
        nil
        (append (task4 (cdr l)) (list (car l)))
        )
    )
;;Задание 5
(defun task5 (x)
    (if (and (>= (list-length x) 2) (<= (list-length x) 4))
        T
        nil
        )
    )
;; #' передает ф-ю
(defun is_list_of_atoms (x)
    (eq (length x) (length (remove-if-not #'atom x))))

(defun task5_1 (x)
    (if (task5 x)
        (is_list_of_atoms x)
        nil
        ))

;;Задание 6
(defun task6 ()
    (setq str (read))
    (read-from-string (concatenate 'string "(" str ")"))
    )

;;Задание 7
(defun task7 ()
    (setq a (list (cons 'vasya  '(("11.12.2014") ("Baker st") ((4.5) (5.0)(4.1)) ((("Andrey")("01.11.1963")("Baker st")("SFU"))(("Masha")("01.06.1963")("Baker st")("AIS")))) )
                  (cons 'natasha  '(("11.12.2015") ("Svobonyi st") ((4.7) (5.0)(4.8)) ((("Alex")("01.11.1956")("Svobonyi st")("KGB"))(("Olga")("01.06.19659")("Svobonyi st")("Labs core")))) )))
    (setq name (read))
    (cadr (assoc name a))
    (caaddr (CADDDR (assoc name a)))
    (/ (+ (caar(CADDDR (assoc name a))) (caadr (CADDDR (assoc name a)))  (caaddr (CADDDR (assoc name a)))) 3)
    )
;;Задание 8
(defun task8 (name)
    (setq a (list (cons 'vasya  '(("11.12.2014") ("Baker st") (4.5 5.0 4.1) ((("Andrey")("01.11.1963")("Baker st")("SFU"))(("Masha")("01.06.1963")("Baker st")("AIS")))) )
                  (cons 'natasha  '(("11.12.2015") ("Svobonyi st") (4.7 5.0 4.8) ((("Alex")("01.11.1956")("Svobonyi st")("KGB"))(("Olga")("01.06.19659")("Svobonyi st")("Labs core")))) )))
    (rplaca (cdddr (assoc name a)) (list (cons 'lec  (first (cadddr(assoc name a)))) (cons 'lab (second (cadddr (assoc name a)))) (cons 'pr (third (cadddr (assoc name a)))) ))
    (assoc name a)
    (/ (+ (cdr (assoc 'lec (fourth (assoc name a)))) (cdr (assoc 'lab (fourth (assoc name a))))  (cdr (assoc 'pr (fourth (assoc name a))))) 3)
    )
