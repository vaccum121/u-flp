;;Задание 1
(LET ((x '(+ 1 2)) (y 'c) ) (LIST x y) )
(LET ((x 'a) (y 'b)) (LET ((z 'c)) (LIST x y z)))
( LET ((x (LET ((z 'a)) z))(y 'b)) (LIST x y))

;;Задание 2
(defun task2 (l)
    (if (null l)
        t
        (if (member (car l) (cdr l))
            nil
            (task2 (cdr l)))
        )
    )
;;Задание 3
(defun and4 (x1 x2 x3 x4)
    (if x1 (if x2 (if x3 (if x4 t nil)))))
(defun or4 (x1 x2 x3 x4)
    (if x1 t (if x2 t (if x3 t (if x4 t nil)))))
;;Задание 4
(defun task4 (coutry)
    (case coutry
      (Russia 'Moscow)
      (USA 'Washington)
      (UK 'London)
      )
    )
;;Задание 5
(defun fact (n)
    (do ((x n (- x 1)) (fac 1))
        ((eq x 1) fac)
        (setq fac (* fac x)))
    )
;;Задание 6
(defun length1(l)
    (prog (len lst)
       (setq len 0)
       (setq lst l)
     tag (if (null lst) (return len))
       (setq len (+ len 1))
       (setq lst (cdr lst))
       (go tag)
       )
    )
