find_word(Word) :-
    read_file_to_codes('/home/vaccum121/dev/eclipse/lab1/src/input.txt', Input, []),
    open('/home/vaccum121/dev/eclipse/lab1/src/out.txt', write, Stream),
    atom_codes(Str, Input),
    tokenize_atom(Str, Str2),
    set_output(Stream),
    search(Str2, Word),
    close(Stream).

search([],_).

search(List, Word) :-
    find(List, Word).

find([],_).

find([Head|Tail], Word) :-
    string_to_list(Head, HList),
    string_to_list(Word, WList),
    prefix(WList, HList),
    write(Head),
    nl,
    find(Tail, Word), !;
    find(Tail, Word).
