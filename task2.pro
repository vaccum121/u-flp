travel(company1, city1, city2, transport1, 100).
travel(company1, city2, city3, transport2, 150).
travel(company2, city1, city3, transport3, 400).
travel(company3, city2, city4, transport4, 800).
travel(company1, city3, city4, transport5, 100).
travel(company2, city5, city4, transport6, 600).
travel(company3, city3, city2, transport7, 200).


canTravel(City1, City2, Cost):-
    canTravel(City1, City2, 0, Cost).

canTravel(City1, City2, Sum, Cost):-
    travel(_, City1, City2, _, Cost1),
    Cost is Sum + Cost1, !;
    travel(_, City1, City3, _, Cost1),
    Sum2 is Sum + Cost1,
    canTravel(City3, City2, Sum2, Cost).


competitors(Company1, Company2):-
    route(City1, City2, Company1),
    route(City1, City2, Company2),
    not(Company1 = Company2).

route(City1, City2, Company):-
    travel(Company, City1, City2, _, _);
    travel(Company, City1, City3, _, _),
    route(City3, City2, Company).


transport(City1, City2):-
    can_travel(City1, City2),
    (travel(_, City1, City2, Tran, _),
     writeln(Tran);
     travel(_, City1, City3, Tran, _),
     writeln(Tran),
     transport(City3, City2)).

can_travel(City1, City2):-
    travel(_, City1, City2, _, _);
    travel(_, City1, City3, _, _),
    can_travel(City3, City2).
