parent(petr, margarita).
parent(petr, iren).
parent(regina, elvira).
parent(regina, iren).

parent(anastasia, anatoliy).
parent(anastasia, aleksey).

parent(margarita, yuri).
parent(nikita, yuri).

parent(iren, vladimir).
parent(iren, aleksey).
parent(anatoliy, vladimir).
parent(anatoliy, aleksey).

parent(nikolay, olga).
parent(nikolay, ylia).
parent(elvira, olga).
parent(elvira, ylia).

parent(yuri, anton).
parent(anna, anton).

parent(vladimir, sofia).
parent(vladimir, alexander).
parent(valentina, sofia).
parent(valentina, alexander).

parent(aleksey, egor).
parent(aleksey, maksim).
parent(natalya, egor).
parent(natalya, maksim).

parent(ylia, andrei).
parent(ilya, andrei).

man(petr).
man(nikita).
man(anatoliy).
man(nikolay).
man(yuri).
man(vladimir).
man(aleksey).
man(ilya).
man(anton).
man(alexander).
man(egor).
man(maksim).
man(andrei).

woman(regina).
woman(iren).
woman(margarita).
woman(iren).
woman(elvira).
woman(anna).
woman(valentina).
woman(natalya).
woman(olga).
woman(ylia).
woman(sofia).

mother(X, Y):-
    parent(X, Y),
    woman(X).

father(X, Y):-
    parent(X, Y),
    man(X).

sister(X, Y):-
    parent(Z, X),
    parent(Z, Y),
    woman(X),
    not(X = Y).

brother(X, Y):-
    parent(Z, X),
    parent(Z, Y),
    man(X),
    not(X=Y).

aunt(X, Y):-
    sister(X, Z),
    parent(Z, Y).

uncle(X, Y):-
    brother(X, Z),
    parent(Z, Y).

grandmother(X, Y):-
    parent(X, Z),
    parent(Z, Y),
    woman(X).

grandfather(X, Y):-
    parent(X, Z),
    parent(Z, Y),
    man(X).
